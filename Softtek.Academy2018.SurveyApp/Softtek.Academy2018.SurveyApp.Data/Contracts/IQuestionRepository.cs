﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Collections.Generic;

namespace Softtek.Academy2018.SurveyApp.Data.Contracts
{
    public interface IQuestionRepository : IGenericRepository<Question>
    {
        ICollection<Question> GetAll();

        bool AddOption(int questionId, int optionId);

        bool Exist(string Text);

        bool Delete(int id);

    }
}
