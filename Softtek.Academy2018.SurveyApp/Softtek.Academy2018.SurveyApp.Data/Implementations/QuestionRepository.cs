﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionRepository : IQuestionRepository
    {
        public int Add(Question question)
        {
            using (var context = new SurveyDbContext())
            {
                question.CreatedDate = DateTime.Now;
                question.ModifiedDate = null;

                context.Questions.Add(question);

                context.SaveChanges();

                return question.Id;
            }
        }

        public bool AddOption(int questionId, int optionId)
        {
            using (var context = new SurveyDbContext())
            {
                Question currentQuestion = context.Questions.SingleOrDefault(x => x.Id == questionId);

                if (currentQuestion == null) return false;

                Option currentOption = context.Options.SingleOrDefault(y => y.Id == optionId);

                if (currentOption == null) return false;

                currentQuestion.Options.Add(currentOption);

                context.SaveChanges();

                return false;
            }
        }

        public bool Delete(int id)
        {
            using (var context = new SurveyDbContext())
            {
                Question currentQuestion = context.Questions.AsNoTracking().SingleOrDefault(x => x.Id == id);

                if (currentQuestion == null) return false;

                currentQuestion.IsActive = false;

                context.Entry(currentQuestion).State = System.Data.Entity.EntityState.Modified;

                context.SaveChanges();

                return true;
            }
        }

        public bool Exist(string Text)
        {
            using (var context = new SurveyDbContext())
            {
                return context.Questions.AsNoTracking().Any(x => x.Text.ToLower() == Text.ToLower());
            }
        }

        public Question Get(int id)
        {
            using (var context = new SurveyDbContext())
            {
                return context.Questions.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public ICollection<Question> GetAll()
        {
            using (var context = new SurveyDbContext())
            {
                return context.Questions.ToList();
            }
        }

        public bool Update(Question question)
        {
            using (var context = new SurveyDbContext())
            {
                Question currentQuestion = context.Questions.SingleOrDefault(x => x.Id == question.Id);

                if (currentQuestion == null) return false;

                currentQuestion.Text = question.Text;
                currentQuestion.ModifiedDate = DateTime.Now;

                context.Entry(currentQuestion).State = System.Data.Entity.EntityState.Modified;

                context.SaveChanges();

                return true;

            }
        }
    }
}
