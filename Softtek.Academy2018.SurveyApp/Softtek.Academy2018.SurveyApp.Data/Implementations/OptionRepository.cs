﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class OptionRepository : IOptionRepository
    {
        public int Add(Option option)
        {
            using (var context = new SurveyDbContext())
            {
                option.CreatedDate = DateTime.Now;
                option.ModifiedDate = null;

                context.Options.Add(option);

                context.SaveChanges();

                return option.Id;
            }
        }


        public bool Exist(string Text)
        {
            using (var context = new SurveyDbContext())
            {
                return context.Options.Any(x => x.Text.ToLower() == Text.ToLower());
            }
        }

        public Option Get(int id)
        {
            using (var context = new SurveyDbContext())
            {
                return context.Options.SingleOrDefault(x => x.Id == id);
            }
        }

        public ICollection<Option> GetAll()
        {
            using (var context = new SurveyDbContext())
            {
                return context.Options.ToList();
            }
        }

        public bool Update(Option option)
        {
            using (var context = new SurveyDbContext())
            {
                Option currentOption = context.Options.SingleOrDefault(x => x.Id == option.Id);

                if (currentOption == null) return false;

                currentOption.Text = option.Text;
                currentOption.ModifiedDate = DateTime.Now;

                context.SaveChanges();

                return true;
            }
        }
    }
}
